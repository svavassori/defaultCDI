
package defaultCDI;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

/**
 *
 * @author sergio
 */
public class Main {

    public static void main(String[] args) throws Exception {

        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        
        // this works but we have to know the specific type, so not really
        // what I wanted
        I ia = container.select(A.class).get();
        I ib = container.select(B.class).get();
        ia.sayHello();
        ib.sayHello();
        
        // To ask for a specific implementation I can use the annotation
        I spec = container.select(I.class, new MyAnn("special")).get();
        spec.sayHello();
        
        // How can I ask for a "default" one?
        // If I specify an annotation, it is similar to specify the type because
        // I need to know it in advance and since we're in the same module/env,
        // using alternatives don't resolve the problem.
        I idefault = container.select(I.class).get();
        idefault.sayHello();
        
        container.shutdown();
    }
}
