
package defaultCDI;

import javax.enterprise.context.ApplicationScoped;

/**
 * This is the "default" implementation.
 * 
 * @author sergio
 */
@ApplicationScoped
public class A implements I {
    
    public void sayHello() {
        
        System.out.println("I'm A");
    }
}
