
package defaultCDI;

import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Named;

/**
 *
 * @author sergio
 */
public class MyAnn extends AnnotationLiteral<Named> implements Named {
    
    private static final long serialVersionUID = 1L;
    private String value;
    
    public MyAnn(String value) {
        
        this.value = value;
    }
    
    public String value() {
        
        return value;
    }
}
