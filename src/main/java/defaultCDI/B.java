
package defaultCDI;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 * This is a special implementation.
 * 
 * @author sergio
 */
@ApplicationScoped
@Named("special")
public class B implements I {
    
    public void sayHello() {
        
        System.out.println("I'm B");
    }
}
